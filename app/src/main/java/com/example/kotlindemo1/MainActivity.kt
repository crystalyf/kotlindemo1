package com.example.kotlindemo1

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.kotlindemo1.databinding.ActivityMainBinding
import rx.Observable

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding:ActivityMainBinding   = DataBindingUtil.setContentView(this,R.layout.activity_main)
        binding.txtShow.text = "hello,kotlin！！！！"
        Observable.from( intArrayOf(1, 2, 3).asIterable())
                  .subscribe {
                      Log.d("tag",it.toString())
                  }
    }
}
